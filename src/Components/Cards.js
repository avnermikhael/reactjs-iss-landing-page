import React from 'react'
import CardItem from './CardItem'
import './Cards.css'

function Cards() {
    return (
        <div className="cards">
            <h1>Check out these news!</h1>
            <div className="cards__container">
                <div className="cards_wrapper">
                    <ul className='cards__items'>
                        <CardItem
                        src='images/img-9.jpg'
                        text='The Alps in Switzerland are pictured from the ISS'
                        label='The Alps in Switzerland'
                        path='/services'
                        />
                        <CardItem
                        src='images/img-2.jpg'
                        text='Expedition 64 Flight Engineer Kate Rubins conducts heart research'
                        label='Research'
                        path='/services'
                        />
                    </ul>
                    <ul className='cards__items'>
                        <CardItem
                        src='images/img-3.jpg'
                        text='Paris, France, the "City of Light"'
                        label='City'
                        path='/services'
                        />
                        <CardItem
                        src='images/img-4.jpg'
                        text='A pair of docked Russian spaceships'
                        label='Spaceships'
                        path='/products'
                        />
                        <CardItem
                        src='images/img-8.jpg'
                        text='The SpaceX Crew Dragon approaches the space station'
                        label='SpaceX'
                        path='/sign-up'
                        />
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Cards
